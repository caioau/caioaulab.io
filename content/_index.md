## > hello, friend <span class="logo__cursor"></span>

---

# Caio Volpato (caioau)

Hey what’s up hello, I have a little [about page](./about/) in English.

Olar! Qual é a boa?

Recomendações: veja [algumas coisas que gosto e recomendo](/recomendacoes)

Acesse via onion: [zgjjgd2o7zh4u76b5d5fpyekeif4nmbtbktufe7z4vaa6sazeraz4nyd.onion](http://zgjjgd2o7zh4u76b5d5fpyekeif4nmbtbktufe7z4vaa6sazeraz4nyd.onion)

---

* A história da vigilância em qualquer sociedade é a história da disputa entre duas tendências: uma concepção positiva de vigilância como necessária para o controle social e uma concepção negativa dela como uma ferramenta usada para limitar liberdade e privacidade.

[Surveillance is a fact of life, so make privacy a human right -- Lawrence Cappello](https://www.economist.com/open-future/2019/12/13/surveillance-is-a-fact-of-life-so-make-privacy-a-human-right)

[Tecnocracia #27 -- Se a vigilância é o novo normal, deveríamos ter o controle dos nossos dados](https://manualdousuario.net/podcast/tecnocracia-27/)
