---
title: "Slides: Introduction to statistics"
date: 2020-02-06T16:10:14-03:00
draft: false
keywords: ["eldorado", "slides", "estatistica", "english"]
tags: ["eldorado", "slides", "estatistica", "english"]
---

Atividade realizada dia 6 fevereiro 2020, no [Eldorado](https://www.eldorado.org.br/)

* [slides](StatsELD2020.pdf) ([codigo fonte dos slides](https://github.com/caioau/caioau-personal/blob/master/website/slides/StatsELD2020.md))
* Jupyter notebook contendo os codigos utilizados: [ELD_Stats.ipynb](https://github.com/caioau/caioau-personal/blob/master/website/slides/ELD_Stats.ipynb)
