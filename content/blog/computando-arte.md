---
title: "Projeto novo: Divulgação cientifica no medium"
date: 2020-11-23T14:00:00-03:00
draft: false
keywords: ["divulgacao-cientifica", "medium", "estatistica", "computacao", "matematica", "ciencia-de-dados", "computando-arte", "rss"]
tags:  ["divulgacao-cientifica", "medium", "estatistica", "computacao", "matematica", "ciencia-de-dados", "computando-arte", "rss"]
---

## Edits

* 21Abr2021: Agora também estamos no [dev.to](https://dev.to/)

---

Juntei com uns amigos e formamos um grupo de divulgação cientifica de estatística, matemática, ciência de dados e computação no medium.

A ideia é publicar um texto por semana.

Confira aqui: [medium.com/computando-arte](https://medium.com/computando-arte)

e no dev.to: [dev.to/computandoarte](https://dev.to/computandoarte)

Assine o feed RSS: [medium.com/feed/computando-arte](https://medium.com/feed/computando-arte) e [dev.to/feed/computandoarte](https://dev.to/feed/computandoarte)

Mandem sugestões, dicas e feedback :)
