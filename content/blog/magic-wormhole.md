---
title: "Precisa transferir um arquivo? O magic-wormhole vai mudar sua vida!"
date: 2021-04-19T14:00:00-03:00
draft: false
keywords: ["dicas", "computando-arte", "python", "magic-wormhole", "texto"]
tags:  ["dicas", "computando-arte", "python", "magic-wormhole", "texto"]
---

Obs.: Originalmente publicado no [computando-arte]({{< ref "/blog/computando-arte" >}}) dia 19Abr2021

Já precisou transferir um arquivo para alguém pela internet e não foi fácil chegar numa solução fácil? O magic-wormhole pode ser uma excelente alternativa!

{{< figure src="xkcd-949.png" caption="[XKCD #949: File transfer](https://xkcd.com/949/)" >}}

Esse XKCD ilustra bem a motivação do magic-wormhole, a fim de copiar um arquivo para outra pessoa, algumas vezes sites como Dropbox não são a melhor opção, e como muitas vezes não se pode encontrar fisicamente para ser via um “pendrive” (principalmente com uma pandemia 😷) o magic-wormhole cria um “túnel mágico” para te salvar✨

## Como usar

O magic-wormhole é um programa em python para ser usado no terminal, então se você já tem o python instalado basta instalá-lo com o pip:

`pip install magic-wormhole`

Uma vez instalado de ambos os lados (quem envia e quem recebe), basta rodar o comando **wormhole send** seguido do nome do arquivo:

{{< highlight bash >}}
$ wormhole send arquivo_a_ser_enviado
Sending 45 Bytes file named 'arquivo_a_ser_enviado'
Wormhole code is: 6-wichita-reindeer
On the other computer, please run:
wormhole receive 6-wichita-reindeer
{{< /highlight >}}

Agora, quem vai receber deve digitar **wormhole receive** seguindo do código gerado (6-wichita-reindeer).

É isso! Como mágica o arquivo é enviado como mágica 🤩

## Como funciona o magic-wormhole?

Sem entrar muito em detalhes, o magic-wormhole funciona em 2 partes: a descoberta e o envio em si.

1. A pessoa que vai enviar o arquivo roda o wormhole send.
2. É gerado um código (ex.: 6-wichita-reindeer), pelo servidor mailbox do magic-wormhole.
3. Quando o cliente entra com o código, a pessoa que envia e a recebe conseguem “se encontrar”.
4. É tentado uma conexão direta, entre as duas pessoas, se estiverem na mesma rede local vai funcionar e ser muito rápido, caso contrário o magic-wormhole intermedia a conexão através do seu servidor de transito (a conexão sempre é criptografada).

Quem tiver interesse para aprender profundamente como funciona veja a [palestra do Brian Warner, o autor original do projeto na PyCon 2016](https://www.youtube.com/watch?v=oFrTqQw0_3c).

Outro programa excelente que funciona de uma maneira similar, mas para outro proposito é o [Syncthing](https://syncthing.net/), que permite sincronizar os arquivos entre seus dispositivos, criando sua “nuvem privada” somente com seus dispositivos.
