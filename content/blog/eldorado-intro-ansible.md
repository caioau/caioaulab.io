---
title: "Slides: Primeiros passos com o Ansible"
date: 2022-08-04T16:00:00-03:00
draft: false
keywords: ["eldorado", "slides", "devops", "linux", "ansible"]
tags:  ["eldorado", "slides", "devops", "linux", "ansible"]
---

Atividade realizada dia 3 agosto 2022, no [Eldorado](https://www.eldorado.org.br/)

* [slides](slides-ansible-eldorado.pdf) ([arquivo markdown editável](https://gitlab.com/caioau/caioau.gitlab.io/-/tree/master/static/blog/eldorado-intro-ansible) feito com [Marp](https://marp.app/)).
