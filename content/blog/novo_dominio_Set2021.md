---
title: 'Novo domínio -> caioau.net'
date: 2021-09-17T14:00:00-03:00
draft: false
keywords: ['self']
tags: ['self']
---

Estou migrando o dominio da minha pagina, para [caioau.net](https://caioau.net/)

O onion continua normalmente :)

Assinem o(s) feed(s) RSS no domínio novo.

Migrei por conta do keybase [estar morto depois da aquisição pela zoom](https://github.com/keybase/client/issues/24303).

Então migrei para Gitlab pages (mesmo repo que usava antes), com o domínio no google domain.

É isso vejo vocês no domínio novo o/
