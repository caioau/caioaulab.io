---
title: "Slides: Primeiros passos com self-hosting (cryptorave 2023)"
date: 2023-05-04T08:00:00-03:00
draft: false
keywords: ["cryptorave", "slides", "devops", "raspberry-pi","selfhost", "linux", "docker", "ansible", "security", "monitoramento"]
tags:  ["cryptorave","slides", "devops", "raspberry-pi","selfhost", "linux", "docker", "ansible", "security", "monitoramento"]
---

Atividade realizada dia 5 maio 2023, na [cryptorave](https://2023.cryptorave.org/).

Palestra baseada no texto [Primeiros passos com self-hosting]({{< ref "/blog/selfhosting" >}})

* [slides](slides-intro-selfhosting_05maio2023.pdf) ([arquivo markdown editável](https://gitlab.com/caioau/caioau.gitlab.io/-/tree/master/static/blog/cr2023-intro-selfhosting) feito com [Marp](https://marp.app/)).
