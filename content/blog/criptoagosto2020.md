---
title: "Atividades realizadas na casa hacker para #CriptoAgosto"
date: 2020-08-19T18:52:53-03:00
draft: false
keywords: ["casahacker", "live"]
tags: ["casahacker", "live"]
---

Em agosto de 2020, a Coalização Direitos na Rede (CDR) (articulação formada por mais de 40 entidades da sociedade civil e acadêmicas em defesa dos direitos digitais), organizou a campanha #CriptoAgosto para mobilizar os usuários sobre a importância da criptografia nas comunicações digitais.

A casa hacker, uma das entidades que pertence a CDR realizou algumas atividades:

* [Live: Tudo o que você queria saber sobre criptografia e não sabia para quem perguntar -- Talita Rodrigues e Cybelle](https://www.youtube.com/watch?v=9CoQpGt6aAg).
* [Live: Criptografia na vida real -- Geraldo Barros e Caio Volpato](https://www.youtube.com/watch?v=ZQTSW5cHDdg).
* [Live: Criptografia é coisa de hacker? -- Jana (ccdpoa) e Cybelle](https://www.youtube.com/watch?feature=youtu.be&v=akOQhg-Qn84)
* [BlogPost: Criptografia : Uma Breve História -- Valentina Buccoliero](https://casahacker.org/hackblog/2020/8/20/criptografia-uma-breve-histria)

Confira o blogpost da CDR com todos conteúdos publicados: [Campanha sobre a importância da criptografia movimenta redes sociais](https://direitosnarede.org.br/2020/09/02/campanha-sobre-a-importancia-da-criptografia-movimenta-redes-sociais/)
