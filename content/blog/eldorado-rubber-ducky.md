---
title: "Slides: Ataques rubber ducky e mitigações"
date: 2023-01-04T16:00:00-03:00
draft: false
keywords: ["eldorado", "slides", "linux", "usbguard", "QubesOS"]
tags:  ["eldorado", "slides", "linux", "usbguard", "QubesOS"]
---

Atividade realizada dia 4 janeiro 2023, no [Eldorado](https://www.eldorado.org.br/)

* [slides](slides-rubber-duck.pdf) ([arquivo markdown editável](https://gitlab.com/caioau/caioau.gitlab.io/-/tree/master/static/blog/eldorado-rubber-ducky) feito com [Marp](https://marp.app/)).
