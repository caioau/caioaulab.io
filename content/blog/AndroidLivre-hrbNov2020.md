---
title: "Slides: (live) Android Livre e Privado para os 99%"
date: 2020-11-11T18:00:00-03:00
draft: false
keywords: ["casahacker", "android", "microg", "lineageos", "fdroid", "slides", "live"]
tags: ["casahacker", "android", "microg", "lineageos", "fdroid", "slides", "live"]
---

Atividade realizada dia 11 novembro, na [Casa hacker](https://casahacker.org/)

* gravacao live: [YouTube](https://www.youtube.com/watch?v=DpFrMr6d70c), [PeerTube](https://peertube.lhc.net.br/videos/watch/9ef9514a-a391-4405-8954-3f2e99a8ae6f)
* [slides](android-CasaHackerNov2020.pdf) ([codigo fonte dos slides](https://github.com/caioau/caioau-personal/blob/master/website/slides/android-CasaHackerNov2020.md))
