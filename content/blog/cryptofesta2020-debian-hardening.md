---
title: "Slides: Segurizando Sistemas: Hardening de Debian"
date: 2019-12-07T16:10:14-03:00
draft: false
keywords: ["Cryptorave", "slides", "Debian","firejail", "usbguard", "QubesOS", "ufw", "linux"]
tags: ["Cryptorave", "slides", "Debian", "firejail", "usbguard", "QubesOS", "ufw", "linux"]
---

Atividade realizada dia 7 dezembro 2019, na [cryptofesta](https://cryptorave.org/)

* [slides](https://0xacab.org/caioau/debian-hardening-install/-/blob/master/slides/debian-hardening-slides-pt-br.pdf) ([codigo fonte dos slides](https://0xacab.org/caioau/debian-hardening-install/-/blob/master/slides/debian-hardening-slides-pt-br.md))
