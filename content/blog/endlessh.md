---
title: 'O que é um tarpit de rede e como colocar um no ar'
date: 2021-01-18T13:00:00-03:00
draft: false
keywords: ["dicas", "computando-arte", "texto", "raspberry-pi","selfhost", "devops", "systemd", "security", "linux"]
tags:  ["dicas", "computando-arte",  "texto", "raspberry-pi", "selfhost", "devops", "systemd", "security", "linux"]
---

Obs.: Originalmente publicado no [computando-arte]({{< ref "/blog/computando-arte" >}}) dia 18Jan2021

{{< figure src="lock-picking.jpeg" width=600px caption="Foto de [Ariel Besagar](https://unsplash.com/@arielbesagar), [Unsplash](https://unsplash.com/photos/Oal07Ai4oTk)">}}

Quando vemos as notícias de cyber ataques, como o WannaCry, é impressionante o impacto desses ataques, infectando milhares de computadores. No caso do WannaCry existia uma atualização de segurança que foi publicada 1 mês antes do ataque, quem não atualizou e estava exposto na internet provavelmente foi infectado. Moral da historia é deve se tomar todas precauções de segurança antes de expor algum computador na Internet. Se tiver interesse, leia as essas estatísticas:

* [Em menos de 1 minuto online esse servidor começou a ser atacado](https://www.zdnet.com/article/this-server-was-online-for-under-a-minute-before-cyber-criminals-started-to-hack-it/)
* [“Quanto tempo até sermos atacados?” Uma lição sobre vulnerabilidade de servidores](https://web.archive.org/web/20200923072723/https://associationsuccess.org/empower/how-long-until-were-hacked-a-lesson-in-server-vulnerability/)

Assim que se coloca algum serviço na internet pública, em poucos minutos, já vemos que os logs de acesso do ssh (protocolo para acessar remotamente computadores) estão lotados de tentativas de combinações de usuário e senha, para conseguir acesso no servidor. Neste texto vamos mostrar como se proteger e como colocar um tarpit no lugar, uma especie de “armadilha” que deixa os atacantes “parados” perdendo tempo e recursos sem atacar ninguém, protegendo quem está vulnerável.

{{< figure src="logs-ssh.jpg" width=700px caption="Logs de acesso do ssh: [Fonte](https://dailydoseoftech.com/how-to-view-failed-ssh-login-attempts-on-linux/)">}}

## Começando pelo começo: protegendo seu servidor ssh

De todas as mitigações para proteger seu servidor ssh, a medida mais efetiva é desativar a autenticação por senha e utilizar apenas a autenticação por chaves privadas.
Se ainda não tem uma chave ssh, gere uma com o comando **ssh-keygen**.

1. Copie sua chave pro seu servidor: `ssh-copy-id -i ~/.ssh/id_rsa usuario@servidor`, se necessário ajuste o caminho da chave depois do -i .
2. Teste se está funcionando: `ssh usuario@servidor` , se o ssh conectar sem pedir senha então está tudo certo.

Desativando autenticação por senha: logue no servidor (`ssh usuario@servidor`), e edite o arquivo /etc/ssh/sshd_config como root (sudo nano /etc/ssh/sshd_config), por fim encontre a linha PasswordAuthentication e coloque no, não se esqueça de não deixar a linha comentada (tire o # no começo da linha). Por fim, reinicie o ssh,  `sudo systemctl restart sshd`

Testando: Tente logar com um usuário que não existe: tipo `ssh usr123321@servidor`, e deve aparecer Permission denied (publickey).

É isso que vai acontecer com quem tentar acessar seu servidor, uma permissão negada sem nem pedir senha.

## endlessh: O que faz um tarpit?

A tradução de Tarpit é “poço de areia movediça”, no nosso contexto de redes é um serviço que intencionalmente insere um atraso, atrasando os clientes que estão conectando e os forçando esperar.

O endlessh é um tarpit de ssh, ele explora um “brecha” na especificação do ssh ([RFC 4253](https://datatracker.ietf.org/doc/html/rfc4253)) que diz que quando um cliente está conectando o servidor pode mandar um “banner” (aqueles avisos legais avisando a politica de segurança), mas a especificação não coloca um limite do tamanho desse banner. É assim que o endlessh funciona, um cliente conectando fica esperando mandar o resto do banner, e o endlessh fica mandando o banner de pouco em pouco só pra manter a conexão ativa. Dessa forma o atacante fica gastando banda e recursos sem acontecer nada ao invés de atacar os clientes que de fato estão inseguros.

{{< figure src="ssh-banner.png" width=700px caption="Exemplo de banner ssh, Fonte: https://www.healthcareitnews.com/news/warning-banners-often-work-heres-how-hospitals-can-use-them-ward-hackers">}}

## Colocando o endlessh no ar

Primeiramente vamos instalar as dependências necessárias:

`sudo apt install git build-essential`

O apt , ou similarmente apt-get, é um gerenciador de pacotes de distribuições Debian. Pense no gerenciador de pacotes como a app store de seu celular, um programa que instala, remove e atualiza os softwares do seu sistema. É uma maravilha, pois imagine que precisa atualizar o VLC, ao invés de ir ao site e baixar o instalador mais recente basta fazer sudo apt update, para verificar se existem atualizações e finalmente sudo apt upgrade para baixar e instalar as atualizações.

O comando acima então instala o git e o pacote build-essential.

Para saber o que um pacote é, podemos utilizar o info do apt, com `apt into build-essential` temos:

```
Package: build-essential
Version: 12.9
Priority: optional
Build-Essential: yes
Section: devel
Maintainer: Matthias Klose <doko@debian.org>
Installed-Size: 20.5 kB
Depends: libc6-dev | libc-dev, gcc (>= 4:10.2), g++ (>= 4:10.2), make, dpkg-dev (>= 1.17.11)
Tag: devel::packaging, interface::commandline, role::data, role::program,
 scope::utility, suite::debian
Download-Size: 7,704 B
APT-Manual-Installed: yes
APT-Sources: <https://deb.debian.org/debian> bullseye/main amd64 Packages
Description: Informational list of build-essential packages
 ...
 This package contains an informational list of packages which are
 considered essential for building Debian packages.  This package also
 depends on the packages on that list, to make it easy to have the
 build-essential packages installed.
 ...

```

Instalamos o build-essential pois ele instala o compilador gcc e o make que vamos utilizar (veja na linha Depends).

Clonando o projeto:

`git clone https://github.com/skeeto/endlessh.git`

Agora precisamos compilar o projeto. Felizmente o projeto tem um makefile, que é um arquivo com os comando de compilações configurados. Dessa forma basta fazer `make` e `sudo make install` para gerar o binário do endlessh e copiar para /usr/local/bin.

Agora pasta rodar o endlessh:

`/usr/local/bin/endlessh -v`

```
2021-01-14T01:22:10.091Z Port 2222
2021-01-14T01:22:10.091Z Delay 10000
2021-01-14T01:22:10.091Z MaxLineLength 32
2021-01-14T01:22:10.091Z MaxClients 4096
2021-01-14T01:22:10.091Z BindFamily IPv4 Mapped IPv6
```

O endlessh está rodando na porta 2222, então abra outra aba no terminal e faça ssh usuario@servidor -p 2222 e veja o que acontece … ou seja nada 😆

Mas como faço pro endlessh iniciar sozinho? Na próxima secção vamos mostrar como :)

## systemd: Como manejar serviços

O systemd é um sistema init, que é o primeiro processo iniciado durante o boot do sistema e ele é responsável por iniciar e gerenciar todos os serviços do sistema. Por exemplo, se está rodando um servidor web com nginx, o systemd vai iniciar o nginx automaticamente e em caso dele travar vai ser iniciado novamente, certificando-se que está sempre no ar.

Na maioria das distribuições Linux, o systemd é o sistema init padrão, que é controlado através do comando systemctl. Por exemplo: veja o status de todos os serviços: `sudo systemctl status --all` . aqui podemos ver a que serviço cada processo está associado.

Mão na massa: na pasta util do endlessh temos o arquivo para subir o endlessh como serviço, o endlessh.service (dentro da pasta util). Não vamos entrar no detalhe das diretivas definidas desse arquivo, aos interessados veja o artigo sobre [systemd na arch wiki](https://wiki.archlinux.org/title/systemd).

Vamos apenas mudar apenas uma coisa no arquivo, na linha exec coloque o -v, para ele rodar mostrando mais detalhes, como os exibir os clientes conectando, ficando assim: ExecStart=/usr/local/bin/endlessh -v

Vamos copiar o arquivo para ser identificado: `sudo cp endlessh.service /etc/systemd/system/`, agora faça `sudo systemctl daemon-reload` para que seja reconhecido, e então finalmente `sudo systemctl enable endlessh`, agora o endlessh está habilitado e deve iniciar automaticamente, para iniciá-lo agora faça `sudo systemctl start endlessh`.

Só para ter certeza, faça `sudo systemctl status endlessh`. Podemos ver o status do serviço

{{< figure src="systemd-status.png" width=700px >}}

Como queríamos, o serviço está rodando.

Também podemos reiniciar o serviço com restart (`sudo systemctl restart endlessh`), e stop para parar. Para desativar temos o disable, porém caso o serviço seja dependência de outro serviço ele pode acabar sendo iniciado. Nesses casos utilize o mask que previne que o serviço seja iniciado. Por fim, temos o reload que faz que com o serviço carregue sua nova configuração sem derrubar o serviço.

## journalctl: Acessando os logs

Agora que o endlessh está rodando e é iniciado automaticamente, como faço para acessar a saída dele para vem quem está conectando?

No systemd os logs de todos serviços podem ser acessados através do comando **journalctl**, faça `sudo journalctl` e veja os logs de todos serviços, inclusive do kernel.

Para vermos apenas do endlessh: `sudo journalctl -u endlessh`

{{< figure src="systemd-logs.png" width=700px caption="logs do endlessh" >}}

Bora analisar os dados? 🤓

Podemos pegar a saída do journalctl e jogar num arquivo para ser lido e parseado para ser colocado num dataframe, por exemplo.

para fazer isso, basta rodar o comando:

`journalctl -u endlessh.service > /tmp/endlessh.log`

Dessa forma os logs estarão em /tmp/endlessh.log. Depois disso basta fazer um programa que lê o arquivo, para fazer suas análises. Mas né, sem tempo irmão 😛. Vamos pela solução simples, usar comandos de processamento de texto, do Unix ✨:

```
sudo journalctl -u endlessh --since 2021-01-15 --until 2021-01-16 | grep CLOSE | wc -l
1044
```

Da mesma forma que o operador > joga a saída pra um arquivo o | (chamado de pipe) redireciona a saída para o outro comando, dessa forma o commando acima conta quantas linhas CLOSE tinha. Ou seja no período de 1 dia tivemos 1044 conexões.

```
sudo journalctl -u endlessh --since 2021-01-15 --until 2021-01-16 | grep CLOSE | awk '{print $8}' | sort | uniq -c | sort -hr
    
    705 host=::ffff:218.92.0.210
     32 host=::ffff:87.251.77.206
     12 host=::ffff:81.161.63.103
     10 host=::ffff:221.181.185.220
      9 host=::ffff:81.161.63.100
      9 host=::ffff:222.187.222.53
      9 host=::ffff:221.181.185.148
      8 host=::ffff:221.181.185.135
      8 host=::ffff:122.194.229.122
      7 host=::ffff:218.92.0.211
      6 host=::ffff:81.161.63.252
      6 host=::ffff:81.161.63.101
[...]
```

O comando acima mostra os IPs que fizeram mais conexões.

## Conclusão

Neste texto aprendemos como proteger seu servidor ssh, dessa forma ninguém não autorizado vai conseguir acessar seu servidor. E mostramos como colocar no ar um tarpit para ajudar a proteger quem ainda está vulnerável. Além disso aprendemos como manejar serviços e fazer processamento de texto de uma forma bem rápida, usando ferramentas Unix, que são tarefas muito importantes na administração de sistemas.

Como continuação dessa jornada de implementar as boas praticas de segurança, sugiro o seguinte tutorial da DigitalOcean: [7 Medidas de Segurança para Proteger Seus Servidores](https://www.digitalocean.com/community/tutorials/7-medidas-de-seguranca-para-proteger-seus-servidores-pt). Para segurança digital pessoal, o vídeo da Eva Galperin é excelente: [Especialista em Internet desmistifica cyber segurança](https://www.youtube.com/watch?v=cQI0O7xdNOU).
