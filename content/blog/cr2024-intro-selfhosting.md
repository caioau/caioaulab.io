---
title: "Slides: Primeiros passos com self-hosting (cryptorave 2024)"
date: 2024-05-07T08:00:00-03:00
draft: false
keywords: ["cryptorave", "slides", "devops", "raspberry-pi","selfhost", "linux", "docker", "ansible", "security", "monitoramento"]
tags:  ["cryptorave","slides", "devops", "raspberry-pi","selfhost", "linux", "docker", "ansible", "security", "monitoramento"]
---

Atividade realizada dia 11 maio 2024, na [cryptorave](https://2024.cryptorave.org/).

Palestra baseada no texto [Primeiros passos com self-hosting]({{< ref "/blog/selfhosting" >}})

* [slides](slides-intro-selfhosting_11maio2024.pdf) ([arquivo markdown editável](https://gitlab.com/caioau/caioau.gitlab.io/-/tree/master/static/blog/cr2024-intro-selfhosting) feito com [Marp](https://marp.app/)).
