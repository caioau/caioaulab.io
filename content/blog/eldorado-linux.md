---
title: "Slides: An intro to setting up Linux servers and maintaining it"
date: 2020-06-11T14:00:00-03:00
draft: false
keywords: ["eldorado", "slides", "linux", "devops", "english"]
tags: ["eldorado", "slides", "linux", "devops", "english"]
---

Atividade realizada dia 11 junho 2020, no [Eldorado](https://www.eldorado.org.br/)

Nessa apresentação interna, mostrei o que considero mais importante saber para fazer o trabalho que venho fazendo de DevOps.

(Obs: Em alguns slides tinham alguns detalhes sensíveis da infra da empresa, nessa versão publicada essas partes foram censuradas, mas isso não prejudica o entendimento do conteúdo)

* [slides](linux_eldorado_11jun2020.pdf) ([arquivo odp editável](linux_eldorado_11jun2020.odp))
