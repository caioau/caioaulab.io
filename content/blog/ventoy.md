---
title: 'Como criar um pendrive de instalação com vários sistemas'
date: 2022-05-08T14:00:00-03:00
draft: false
keywords: ["computando-arte", "texto", "linux"]
tags: ["computando-arte", "texto", "linux"]
---

# Conheça o Ventoy, a ferramenta que nos ajuda a fazer isso

Obs.: Originalmente publicado no [computando-arte]({{< ref "/blog/computando-arte" >}}) dia 9Maio2022

Quando vou ajudar alguém a formatar seu computador tenho esse problema, preciso levar vários pen drives preparados dependendo de qual sistema a pessoa quer instalar (como o Windows, o Ubuntu, PopOS, Manjaro e etc …). Isso acontece porque só dá para ter um sistema operacional por pen drive.

Hoje vou falar de uma solução desse problema, criar um pen drive multiboot contendo vários sistemas em um único pen drive, utilizando o Ventoy 🤯

{{< figure src="feature_pic.jpg" width=600px caption="Marvin Meyer, on [unsplash](https://unsplash.com/photos/SYTO3xs06fU)" >}}

## Domando o destruidor de discos (dd)

Antes de mostrar o esquema multiboot, vou mostrar o esquema tradicional de gravar um pendrive usando o tradicional comando dd no Linux. 

Pra quem usa Windows calma aí que já mostro outra opção.

A tarefa de gravar um pendrive bootável com o dd consiste de 6 tarefas, são elas:

1. Baixar a imagem do sistema operacional.
2. Verificar se a imagem não está corrompida ou adulterada.
3. Plugando o pendrive e identificando onde “subiu”.
4. Fazer um umount no pendrive.
5. De fato, gravando a imagem com o dd.
6. Removendo o pendrive com segurança.

---

1. Antes de tudo precisamos baixar a imagem .iso, que podemos encontrar no site do Ubuntu ([releases.ubuntu.com](https://releases.ubuntu.com/)). Usei o link abaixo:

{{< highlight bash >}}
wget https://releases.ubuntu.com/20.04.4/ubuntu-20.04.4-desktop-amd64.iso
{{< /highlight >}}

2. É de bom tom sempre verificar se imagem iso não está corrompida ou foi adulterada ;) , com este intuito vamos importar a chave de assinatura do ubuntu:

{{< highlight bash >}}
gpg --keyid-format long --keyserver hkp://keyserver.ubuntu.com --recv-keys 0x46181433FBB75451 0xD94AA3F0EFE21092
{{< /highlight >}}

Veja que as chaves foram importadas com sucesso:

{{< highlight bash >}}
gpg: key D94AA3F0EFE21092: public key "Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>" imported
gpg: key 46181433FBB75451: public key "Ubuntu CD Image Automatic Signing Key <cdimage@ubuntu.com>" imported
gpg: Total number processed: 2
gpg:               imported: 2
{{< /highlight >}}

Em seguida vamos baixar os hashes da imagem e a assinatura:

{{< highlight bash >}}
wget https://releases.ubuntu.com/20.04.4/SHA256SUMS
wget https://releases.ubuntu.com/20.04.4/SHA256SUMS.gpg
{{< /highlight >}}

Verificando a assinatura:

{{< highlight bash >}}
gpg --verify SHA256SUMS.gpg SHA256SUMS
{{< /highlight >}}

Deu bom! Repare no resultado do comando (good signature ...)

{{< highlight bash >}}
gpg: Signature made qui 24 fev 2022 17:36:20 -03
gpg:                using RSA key 843938DF228D22F7B3742BC0D94AA3F0EFE21092
gpg: Good signature from "Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>" [unknown] # <--------
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 8439 38DF 228D 22F7 B374  2BC0 D94A A3F0 EFE2 1092
{{< /highlight >}}

Por fim, verificando a imagem:

{{< highlight bash >}}
sha256sum --ignore-missing -c SHA256SUMS
ubuntu-20.04.4-desktop-amd64.iso: OK
{{< /highlight >}}

3. Tá safe! Bora gravar essa imagem. Para isto, antes de plugar o pen drive execute o comando lsblk para listar os dispositivos de armazenamento, então plugue o pendrive e execute o lsblk novamente e veja se “onde o pendrive subiu” (comparando quando rodou o lsblk a primeira vez), isto é, se foi conectado ao sdb, sdc ou sdd etc … Aqui “subiu” como sdb.

4. Sempre bom fazer umount antes:

{{< highlight bash >}}
sudo umount /dev/sdb*
{{< /highlight >}}

5. Finalmente gravando a imagem:

{{< highlight bash >}}
sudo dd if=ubuntu-20.04.4-desktop-amd64.iso of=/dev/sdb bs=1M status=progress ; sync
{{< /highlight >}}

Aponte para o endereço da imagem no argumento if, e aponte para o endereço do pen drive no argumento of. Preste atenção se de fato está apontando para o pen drive em /dev/sdb, não coloque a partição /dev/sdb1 ou 2, é só sdb!

Observação: O comando sync faz com que a imagem completa seja de fato escrita no pendrive, não somente no cache.

6. Espere a gravação finalizar (demora alguns minutos) e por fim vamos remover com segurança o pendrive:

{{< highlight bash >}}
sudo udisksctl power-off -b /dev/sdb
{{< /highlight >}}

## Gravando utilizando o etcher

Uma alternativa ao dd é utilizar o etcher, que está disponível no Windows, Mac e Linux, super simples de usar! Basta entrar no site [balena.io/etcher/](https://www.balena.io/etcher/) e baixar o programa, então selecionar a imagem iso, o pen drive e clicar em Flash!, conforme a figura abaixo:

{{< figure src="print_ether.png" width=600px >}}

## Multiboot com Ventoy

Chegou a hora de falar do Ventoy \o/

Instalar o ventoy é tranquilo, basta ir na pagina de releases no Github do Ventoy [github.com/ventoy/Ventoy/releases/](https://github.com/ventoy/Ventoy/releases/), estou utilizando a versão 1.0.73:

{{< highlight bash >}}
wget https://github.com/ventoy/Ventoy/releases/download/v1.0.73/ventoy-1.0.73-linux.tar.gz
{{< /highlight >}}

Verificando o download:

{{< highlight bash >}}
sha256sum ventoy-1.0.73-linux.tar.gz | grep ba1b61864598af2db662e393043c8759213869419ad95adb71fb5d92484f4b7d
{{< /highlight >}}

Extraindo o tar:

{{< highlight bash >}}
tar xvf ventoy-1.0.73-linux.tar.gz
{{< /highlight >}}

Gravando o ventoy:

{{< highlight bash >}}
sudo umount /dev/sdb*
cd ventoy-1.0.73/
sudo ./Ventoy2Disk.sh -i /dev/sdb
{{< /highlight >}}

Quando finalizar a gravação, basta copiar as isos para uma partição que foi criada e sucesso.

{{< figure src="print_particao.png" width=600px >}}

Quando ligar o computador, e apertar F12 (ou a tecla do seu fabricante) com esse pendrive a tela do ventoy vai aparecer, no qual você pode escolher qual das imagens será utilizada.

{{< figure src="print_ventoy.png" width=600px caption="Captura de tela do Ventoy, Fonte: [ventoy.net/en/screenshot.html](https://www.ventoy.net/en/screenshot.html)" >}}

Quando precisar de outro sistema basta colocar a iso na partição do ventoy e usar! Muito prático!

Testei e funcionou bootar tanto no modo legacy (MBR) quanto UEFI ^_^

Adorei o Ventoy :) daqui para frente só preciso de um pendrive na minha caixa de ferramentas quando for atender alguém.
