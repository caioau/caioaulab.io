---
title: "Slides: (live) O elo mais fraco da nossa segurança: S&nh5s"
date: 2020-06-23T16:10:14-03:00
draft: false
keywords: ["Encripta", "diceware", "passwords", "2FA", "password-manager","slides", "live"]
tags: ["Encripta", "diceware", "passwords", "2FA", "password-manager","slides", "live"]
---

Live realizada dia 23 junho 2020, na [CriptoGoma](https://encripta.org/)

* gravacao live: [YouTube](https://youtu.be/tRQP2B3rBNc), [PeerTube](https://kolektiva.media/videos/watch/4cf3ea1b-9798-4d20-9fa0-e928d785b793)
* [slides](SenhasCryptoGama2020.pdf) ([codigo fonte dos slides](https://github.com/caioau/caioau-personal/blob/master/website/slides/SenhasCryptoGama2020.md))
