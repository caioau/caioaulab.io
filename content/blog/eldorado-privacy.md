---
title: "Slides: Online privacy and security guide"
date: 2019-05-23T16:10:14-03:00
draft: false
keywords: ["eldorado", "slides", "privacidade", "english"]
tags: ["eldorado", "slides", "privacidade", "english"]
---

Atividade realizada dia 23 maio 2020, no [Eldorado](https://www.eldorado.org.br/)

* [slides](eldPrivacySec.pdf) ([codigo fonte dos slides](https://github.com/caioau/caioau-personal/blob/master/website/slides/eldPrivacySec.md))
