---
title: 'Termux: Um terminal linux no seu Android'
date: 2021-09-10T14:00:00-03:00
draft: false
keywords: ["devops", "portal-embarcados", "texto", "android", "fdroid", "linux", "python"]
tags: ["devops", "portal-embarcados", "texto", "android", "fdroid", "linux", "python"]
---

Originalmente publicado no [portal embarcados](https://www.embarcados.com.br/termux-um-terminal-linux-no-seu-android/) dia 8 setembro de 2021, com apoio dos colegas do instituto Eldorado.

Hoje vamos falar do **termux**: um app open source para android que traz um terminal completo Linux com diversas possibilidades e facilidades, como fazer backups dos arquivos do Android e programar com python 🤓

{{< figure src="termux.png" width=400px >}}

## Instalando o app: O que é a Fdroid?

O **termux** atualmente está apenas disponível na Fdroid, não é possível instalar a versão mais recente na play store. Mas afinal o que é a Fdroid?

{{< figure src="fdroid.png" width=200px caption="Logo da Fdroid">}}

A Fdroid é uma loja de apps para Android, tipo a Play Store, mas apenas com apps software livre/open source. Atualmente existem mais de 3 mil apps oferecidos lá, dê uma olhada se tem algum que te interesse.

Para usar a Fdroid basta ir no site [fdroid.org](http://fdroid.org/) clique em download e instale o apk.

Com a Fdroid instalada, busque pelo **termux** e instale-o de lá, a Fdroid periodicamente vai procurar por atualizações dos seus apps.

## Instalei o Termux, e agora?

Com o **termux** instalado, podemos instalar alguns pacotes. Para fazer isso basta usar o comando pkg igual faria com apt ou apt-get (não precisa de sudo).

Então antes de tudo vamos atualizar os repos, para isto execute a  seguinte linha de comando: `pkg update` .

Espere terminar e aceite atualizar os pacotes.

Agora vamos instalar o neofetch usando a linha de comando: `pkg install neofetch`.

Por fim rode o **neofetch**, ele exibirá o logo do Android (que no caso é a nossa “distro” Linux) e diversas informações como número de pacotes instalados, qual kernel, etc.

{{< figure src="meme.png" width=500px caption="Créditos: [@anubhavitis](https://twitter.com/anubhavitis/status/1411935066406625287/photo/1)">}}

## Acessando remotamente o celular com ssh

Como o teclado e a tela do celular são pequenos, uma possibilidade seria usar um teclado externo bluetooth ou conectado no cabo OTG. Outra abordagem possível é acessar remotamente o **termux** via ssh com um computador na mesma rede. Para isto:

1. Instale o **openssh**: `pkg install openssh`
2. Crie uma senha para logar com o comando: `passwd`
    * Digite a senha 2 vezes (é normal não aparecer nada enquanto digita a senha).
3. Inicie o servidor ssh com o comando: `sshd`
4. Descubra o IP do android: na cortina de notificações pressione e segure o ícone do WiFi, clique na rede conectada, avançado, e o seu endereço IP será apresentado no formato 999.999.9.9999
5. Agora no computador dispare o ssh no IP, porém na porta 8022 usando o comando: `ssh <user>@<endereco_IP> -p 8022` . E digite a senha que configurou antes, para quem está no Windows uma possibilidade é instalar o **[WSL](https://docs.microsoft.com/pt-br/windows/wsl/install-win10)** e ter um linux dentro do Windows ou usar o **[MobaXterm](https://mobaxterm.mobatek.net/)**.

Prontinho! Agora é possível acessar remotamente o **termux** no computador via ssh. Porém logo vai perceber que o terminal começa a ficar lento, ou congela e as vezes volta. Isso acontece porque o Android está com a tela apagada e entrando num modo de “economia de energia”, nesses casos temos a opção do **termux** usar wakelocks para prevenir que o Android suspenda, isso vai consumir mais bateria mas se tudo bem basta abaixar a cortina de notificação e clicar em acquire wakelock.

Dica 1: O seu roteador WiFi provavelmente atribui os endereços dos dispositivos de forma sequencial, ou seja, pode ser que o IP do Android amanhã não seja mais aquele que vimos anteriormente. Vá na configuração do seu roteador e reserve um IP fixo para o Android. Segue abaixo a opção em um roteador D-Link:

{{< figure src="router.png" width=300px >}}

Dica 2: Criando um alias no ssh: toda vez que vamos acessar o android temos que digitar tudo isso: `ssh <user>@<endereco_IP> -p 8022`, vamos criar um apelido que simplifica o comando para: `ssh android` . No computador edite o arquivo `~/.ssh/config` colocando o seguinte:

{{< highlight bash "linenos=false">}}
Host android
  HostName <endereco_IP>
  User user
  Compression yes
  Port 8022

{{< / highlight >}}

## Instalando o python e jupyter notebook 🤯

Podemos instalar no nosso android um ambiente de computação científica python para fazer análises, testar coisas e levar pra onde quisermos. Para isto, basta executar as seguintes linhas de comando no **termux**:

{{< highlight bash "linenos=true">}}
pkg install python build-essential libzmq freetype libjpeg-turbo libpng
python3 -m venv venv
source venv/bin/activate
pip3 install -U pip wheel setuptools
pip3 install jupyter
{{< / highlight >}}

Estas linhas de comando instalam o python e algumas dependências necessárias para instalar as bibliotecas do python. Em seguida, foi criado um “ambiente virtual” (**venv**) que permite instalar nossas bibliotecas de forma separada, para ter um ambiente python separado para cada projeto. Toda vez que quisermos usar o **venv** é necessário executar `source venv/bin/activate`, responsável por ativar o **venv** no shell atual do **termux**(linha 3). Nas linhas seguintes, 4 e 5, atualizamos nossas ferramentas do python (pip, wheel e setuptools) e por fim, instalamos o jupyter notebook.

jupyter notebook é iniciado com:

`jupyter notebook --ip=0.0.0.0`

Coloque o link no navegador e substitua pelo IP do android e pronto, tudo funcionando:

{{< figure src="jupyter_notebook.png" width=600px >}}

Sucesso! O jupyter notebook já está funcionando perfeitamente. É esperado que a instalação de pacotes python, como o numpy (usando o comando `pip install`) demore mais do que em uma máquina Linux convencional. Por que? O python distribui suas bibliotecas com os wheels que são pacotes pré-compilados mais rápidos de serem instalados, porém os wheels foram gerados para a arquitetura de processadores x86_64 e estamos usando um processador arm no nosso android então temos que compilar tudo. Outra coisa é que além da arquitetura os wheels são gerados para uma determinada biblioteca padrão a **glibc** mas o android usa outra libc, a **bionic**. Na wiki do **termux** tem uma pagina com todas as diferenças de uma maquina linux convencional: [https://wiki.termux.com/wiki/Differences_from_Linux](https://wiki.termux.com/wiki/Differences_from_Linux)

## Acessando os arquivos do celular

Podemos acessar as fotos e outros arquivos do celular pelo **termux** executando o comando: `termux-setup-storage.` É necessário  permitir o acesso de arquivos pelo app. Podemos ver quais pastas estão consumindo mais espaço:

```
cd /sdcard
du -shxc * | sort -rh
4.0G	total
2.0G	DCIM
828M	Android
704M	Pictures
231M	Telegram
213M	syncthing
103M	Download
```

O comando acima (`du`) lista o espaço utilizado por cada pasta e em seguida pegamos sua saída para o comando `sort` através do` |` (chamado de pipe), para ordenar de forma decrescente. Outro comando interessante é o `df` que exibe o uso de disco de cada “partição” e quanto está livre.

Outra coisa legal de fazer agora que estamos acessando os arquivos do celular é deletar fotos (ou arquivos) de forma segura. Quando deletamos um arquivo, este não é removido de fato, simplesmente o espaço daquele arquivo é marcado como livre, dessa forma é possível recuperar arquivos deletados. Então para deletar um arquivo de forma segura temos que sobrescrever o conteúdo do arquivo com “lixo” para que o conteúdo original não seja recuperado, para fazer isso temos o comando `shred`, basta executar: `shred -u <nome_arquivo>`, como por exemplo:

`shred -uv IMG_20210416_103105401.jpg`

Outro comando, mais completo que o `shred` é o `srm`. Para instalar execute `pkg install secure-delete.` Ele permite deletar diretórios de forma recursiva e outras opções não presentes no `shred`.

## Sincronizando arquivos com o PC

O protocolo ssh além de permitir acessar remotamente o Android, permite transferir arquivos. O comando que faz essa cópia é o scp, para copiar um arquivo basta executar no terminal do PC: `scp arquivo.jpg user@<endereco_IP>:/sdcard/DCIM/`, com o usuário, IP e caminho de destino.

Outro comando muito usado para essa tarefa é o `rsync`, ele permite sincronizar pastas entre o Android e o PC. Enquanto o comando scp vai “re copiar” todas as fotos para o PC, o rsync vai copiar apenas as novas e o que mudou, sendo na maioria das vezes mais eficiente. Primeiro vamos instalar o rsync no termux (o rsync precisa estar instalado tanto no PC quanto no android):

`pkg install rsync`

O **rsync** oferece várias opções, mas nesse caso o importante é: `rsync -avh -P user@<endereco_IP>:/sdcard/DCIM/ fotos_celular/.` Na primeira vez que for executado, esse comando vai copiar todas e fotos para o PC, e das próximas vezes copiará somente o que mudou.

Por fim vou indicar um app ótimo para sincronizar os arquivos entre o celular e pc: o [syncthing.net](https://syncthing.net/), tem na Fdroid e com ele é possível fazer backup de todos meus arquivos do Android no PC.

## Alguns apps para terminal

Para quem vive no terminal tem alguns apps que ajudam a realizar algumas tarefas e para quem não usa muito a interface gráfica.

Analisador de espaço: o **ncdu.** Da mesma forma que usamos o **du** para ver quais pastas estão usando mais espaço, o **ncdu** faz a mesma coisa, mas permite navegar de forma rápida e deletar os arquivos/pastas para liberar espaço.

{{< figure src="ncdu.png" width=600px >}}

Para navegar nas pastas temos o **ranger**: um gerenciador de arquivos como o explorer do Windows ou **nautilus** no Linux (gnome).

{{< figure src="ranger.png" width=600px >}}

Por fim, um agregador de feeds RSS: o **newsboat**, o qual permite acompanhar os feeds RSS:

{{< figure src="newsboat.png" width=700px >}}

Feed do [Computando Arte – Medium](https://medium.com/computando-arte)

## Considerações finais

Neste texto tentei mostrar para nosso leitor sobre o que dá para fazer com **termux** e um terminal linux, ilustrando sobre como realizar as tarefas apenas no terminal. Para quem gostou do conceito de acessar remotamente os apps e arquivos, mas achou android um pouco lento, confira esse blogspot da linuxserver.io sobre a imagem webtop que fizeram: [linuxserver.io/blog/2021-05-05-meet-webtops-a-linux-desktop-environment-in-your-browser](https://www.linuxserver.io/blog/2021-05-05-meet-webtops-a-linux-desktop-environment-in-your-browser) fornecendo uma alternativa mais rápida.
