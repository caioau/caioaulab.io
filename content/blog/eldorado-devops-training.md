---
title: "Slides: Treinamento de DevOps"
date: 2020-10-25T16:00:00-03:00
draft: false
keywords: ["eldorado", "slides", "linux", "devops", "systemd", "security", "monitoramento", "docker", "ansible", "english"]
tags:  ["eldorado", "slides", "linux", "devops", "systemd", "security", "monitoramento", "docker", "ansible", "english"]
---

Atividade realizada nos dias 5 até 9 outubro 2020, no [Eldorado](https://www.eldorado.org.br/)

Nesse treinamento de 5h, apresentei diversos conceitos e ferramentas de DevOps: como manejar serviços systemd, rede e firewall, como criar seu pacote Debian para deploy mais eficiente, security, monitoramento, backups, docker e ansible.

* [slides](devops_eldorado_5oct2020.pdf) ([arquivo odp editável](devops_eldorado_5oct2020.odp)).
* [arquivos extras usados na talk](devops_eldorado_5oct2020.zip)
