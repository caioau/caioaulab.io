---
title: "Slides: Como operar uma infra on premisses"
date: 2021-12-15T08:00:00-03:00
draft: false
keywords: ["eldorado", "slides", "devops", "raspberry-pi","selfhost", "linux", "docker", "ansible", "security", "monitoramento"]
tags:  ["eldorado", "slides", "devops", "raspberry-pi","selfhost", "linux", "docker", "ansible", "security", "monitoramento"]
---

Atividade realizada dia 15 dezembro 2021, no [Eldorado](https://www.eldorado.org.br/)

Palestra baseada no texto [Primeiros passos com self-hosting]({{< ref "/blog/selfhosting" >}})

* [slides](slides-intro-infra_caioau_15dez2021.pdf) ([arquivo markdown editável](https://gitlab.com/caioau/caioau.gitlab.io/-/tree/master/static/blog/eldorado-intro-infra) feito com [Marp](https://marp.app/)).
