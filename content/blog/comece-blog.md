---
title: 'Por que e como fazer um blog técnico'
date: 2022-03-21T14:00:00-03:00
draft: false
keywords: ["computando-arte", "texto", "medium", "divulgacao-cientifica", "rss"]
tags: ["computando-arte", "texto", "medium", "divulgacao-cientifica", "rss"]
---

Obs.: Originalmente publicado no [computando-arte]({{< ref "/blog/computando-arte" >}}) dia 21Março2022

Hoje vou falar de um hábito inesperado que passei a praticar durante a quarentena, o de compartilhar o conhecimento através de textos.

Eu não era um bom aluno de português :/ mas hoje percebo a importância de conseguir se comunicar bem, e como faz toda diferença!

Antes da pandemia, participava de algumas comunidades, hackerspaces e eventos (como a Cryptorave e a CasaHacker), e nesses espaços físicos são construídas diversas atividades colaborativamente.

Edai veio a pandemia que mudou essas interações então comecei a criar esses registros dessas trocas.

{{< figure src="feature_pic.jpg" width=600px caption="Mika Baumeister, [unsplash](https://unsplash.com/photos/LaqL8nxiacc)" >}}

## Acompanhe referências da sua área

O primeiro passo para começar a escrever é ler ;) quem escreve bem normalmente lê muito e na comunicação científica isso não é diferente. Veja o que outras pessoas falam, procure e acompanhe blogs e talks dessas pessoas. Preste atenção em alguns pontos: Como elas explicam determinado conceito? Quais analogias/comparações ela usa? E, principalmente, como apresentar isso para quem está começando e quais detalhes são importantes?

## Decida se quer ser anônimo ou não

Uma possibilidade é escrever sem se identificar, dessa forma você não “contamina” o debate com quem você é, onde você trabalha, onde você estudou, com que você interage, sua aparência, enfim, de onde você vem.

Um exemplo disso é o [Startup da Real](https://startupdareal.medium.com/), ele criou um perfil para expor os discursos de empreendedorismo. Depois de produzir vários textos aqui no medium ele amarrou esses textos em um livro chamado “Este livro não vai te deixar rico: Descubra a verdade sobre empreendedorismo, startups e a arte de ganhar dinheiro”. O startup da real, ou Alberto Brandão, revelou sua identidade no talk show do Ronald Rios. Confira aqui ele contando a história: [STARTUP DA REAL - Ronald Rios Talk Show #12](https://www.youtube.com/watch?v=Aca1ZbHDQ3g)

Apesar dessas vantagens, vou trazer no próximo tópico um bom motivo para não fazer de forma anônima.

## Use seu conteúdo para construir rastros digitais positivos

A internet não esquece! Já deu um google no seu nome completo? O que encontrou? Possivelmente seu currículo lattes ou algum trabalho publicado, talvez até seu perfil no LinkedIn. Mas às vezes tem coisas que você não gostaria que aparecesse (pelo menos com tanto destaque), como por exemplo um processo no JusBrasil, no Diário Oficial ou então alguma nota em disciplina da faculdade. E o mais triste disso é que não temos muito como deletar essas pegadas digitais de forma fácil e rápida.

Ao invés de tentar deletar toda sua presença online e ocupar esses espaços online da forma mais anônima possível, que tal criar um conteúdo legal e que quando pesquisarem você isso vai causar uma ótima primeira impressão?

Quando pesquisarem seu nome no Google vão encontrar seu artigo, seu blog pessoal, uma talk que você fez em algum evento, uma participação em podcast ou live. E as coisas que não tem tanto orgulho só irão aparecer nas últimas páginas da pesquisa, que normalmente as pessoas não procuram.  Vi essa excelente proposta no texto da Casey Fiesler, ela estuda os espaços online há anos, e propôs essa abordagem no contexto da hashtag #10YearChallenge, quando acabaram trazendo à tona coisas que a pessoa postou e que hoje em dia não se orgulham. Veja o texto dela: [If you're worried about your digital footprint, don't destroy it — make it bigger. | by Casey Fiesler](https://cfiesler.medium.com/if-youre-worried-about-your-digital-footprint-don-t-destroy-it-make-it-better-cce67263c587)

## Outros porquês para divulgar conhecimento

Além de ter uma presença online mais positiva, quais outras vantagens de divulgar conhecimento?

Uma habilidade que você acaba desenvolvendo é de colocar em uma ordem linear como abordar determinado assunto. No fundo, você vai escrever uma história com começo, meio e fim. E quando você registra isso logo depois que começou a aprender um assunto novo, vai te ajudar bastante no seu processo de aprendizado.

Quando criar uma documentação quando você tiver que configurar seu ambiente, se você documentar vai ficar mais fácil de reproduzir quando precisar novamente. Como você já escreveu essa documentação, terá pouco trabalho para publicar e vai ajudar quem estiver precisando. Por exemplo, quer aprender como montei meu setup de backups automatizados? Confira aqui: [Como parei de me preocupar e passei a adorar minha solução de backups](https://caioau.net/blog/backups/).

## Ninguém se importa com seu conteúdo

E isso é bom! Esse post do Gabs Ferreira, do grupo Alura: [Ninguém se importa com o seu conteúdo: e isso é bom - Gabs Ferreira](http://gabsferreira.com/ninguem-se-importa-com-o-seu-conteudo-e-isso-e-bom/) trouxe essa perspectiva, só vai!

Se estiver com receio de começar a escrever, pois vão te criticar ou te julgar de alguma forma, não se preocupe! A maioria das pessoas nem vai consumir seu conteúdo. Então escreva pensando em você, como isso vai te ajudar a aprender melhor, a se expressar melhor, a colocar as coisas em uma ordem linear e etc … E se conseguir impactar positivamente mesmo que for apenas uma pessoa isso já valeu a pena.

Pra quem curte estatísticas, na pegada do princípio de pareto (80% dos efeitos vêm de 20% das causas) temos a regra [90-9-1 (ou regra do 1%)](https://en.wikipedia.org/wiki/1%25_rule) que mostra que via de regra para qualquer comunidade na internet 90% dos internautas vão apenas consumir de forma passiva, sem interagir de nenhuma forma (os “Lurkers”). Dos 10% restantes, 9% vão participar pouco através das “micro-reações”, como like ou claps no medium, e apenas 1% dos internautas serão responsáveis pela maioria das discussões.

Na reportagem de Janeiro de 2012 da Revista Galileu ([web archive](https://web.archive.org/web/20120102101719/http://revistagalileu.globo.com/Revista/Common/0,,EMI281132-17773,00-OS+COMENTADORES+DA+INTERNET.html)) entrevistou uma pessoa que foi responsável por mais de CINCO MIL comentários, de forma anônima, na folha.com.

{{< figure src="xkcd_duty_calls.png" width=400px caption="[xkcd #386](https://xkcd.com/386/)" >}}

## Melhor escrever em português ou inglês?

A língua é uma barreira! Com isso em vista, devo escrever em português ou em inglês?

Se optar pelo inglês, possivelmente seu conteúdo vai atingir mais pessoas, mas em compensação tem muito conteúdo em inglês então fica mais difícil do seu texto se destacar.

Caso seja algo específico, como um tutorial de um framework recém lançado, talvez seja melhor fazer em inglês.

## Alguns exemplos

Vou trazer alguns textos de formatos diferentes para trazer inspirações.

O primeiro exemplo é uma dica de ouro, é o texto da Anna sobre [Como ativar o cancelamento de eco no PulseAudio — Anna escreve](https://anna.flourishing.stream/pt-br/today-i-learned/cancelamento-de-ruido-pulseaudio/). No texto, a autora apresenta o problema (a captação de áudio do microfone embutido do notebook não funciona bem) e apresenta como atacar o problema.

Outro formato é falar sobre seu setup (um site que gosto muito de acompanhar é o [manualdousuario.net](https://manualdousuario.net/)).No tipo de texto como [O escritório em casa da designer de UI/UX Camila](https://manualdousuario.net/home-office-camila/),você vai falar como é o seu escritório, quais equipamentos tem e como usa. É bem divertido e ajuda quem está em dúvida do que comprar, ou até mesmo repensar a forma de trabalhar.

Para fechar os exemplos vou falar do texto do Gabriel Arruda que eu adoraria ter lido na faculdade quando estudava sobre o assunto. O texto se chama [Organizando férias com otimização](https://gdarruda.github.io/2021/04/04/ferias-otimizadas.html). A proposta é encontrar os melhores períodos para entrar de férias para ter o máximo de dias de descanso. No texto o autor fala do problema, como vai ser modelado, a linguagem [MiniZinc](https://www.minizinc.org/), o código e o resultado. Atente-se em como o autor conta uma história, com começo, meio e fim, e tenha isso em mente quando estiver escrevendo seu texto.

## Use memes

Uma imagem vale por mil palavras, para deixar seu textão mais atraente e prender a atenção dos leitores, use e abuse de memes!

Por exemplo se estiver escrevendo sobre como a mediana é mais uma medida mais robusta que a média a valores aberrantes (outliers), o meme abaixo encaixa como uma luva:

{{< figure src="meme_outliar.png" width=500px >}}

Entretanto em alguns contextos mais formais não pega muito bem usar memes :/ nesses casos sugiro usar outros recursos visuais como diagramas ou tirinhas como do XKCD. Uma obra prima foi o texto aqui no computando arte [Métricas de Modelos Preditivos: Acurácia em Problemas de Classificação Desbalanceados | by Mikael Souza | Computando Arte](https://medium.com/computando-arte/entendendo-m%C3%A9tricas-de-modelos-preditivos-problemas-de-classifica%C3%A7%C3%A3o-desbalanceados-11aa406f6aac)

{{< figure src="meme_acuracia.png" width=900px >}}

## Onde escrever

O Medium é a opção mais conhecida para publicar textos. Nele seu texto é “impulsionado” para pessoas que estão na plataforma, porém a desvantagem é que ele tem um paywall que impede a leitura de mais de 5 textos por dia :/

{{< figure src="paywall.png" width=500px caption="[@linhadotrem/Twitter](https://twitter.com/linhadotrem/status/1451596753032069125)">}}

Uma plataforma mais focada para o público de tecnologia é o [dev.to](https://dev.to/), nele os textos devem estar no formato markdown, ao passo que o Medium tem um editor próprio. Mas o dev.to não tem paywall \o/

Dependendo do seu objetivo, uma opção é escrever e publicar no próprio Linkedin. Para isso, você precisa habilitar o modo criador ([instruções](https://www.linkedin.com/help/linkedin/answer/a522537/linkedin-creator-mode?lang=pt)) então no seu perfil aparece sobre quais tópicos você escreve. Um exemplo é a Vanessa Moura (WonderWanny).

{{< figure src="linkedin_creator.png" width=600px caption="[linkedin.com/in/vannessamoura](https://www.linkedin.com/in/vannessamoura/)">}}

_Qual plataforma devo escolher?_ Minha sugestão é que não se preocupe muito com isso, só comece a escrever! Mas sugiro que tenha um blog pessoal pelo menos como “backup” para não ficar refém das plataformas.

## Blog pessoal

Poucas plataformas da internet sobreviveram à prova do tempo. Lembra do Orkut? Já teve um MSN? Ter um blog pessoal te ajuda a evitar isso, vou dar algumas dicas de ter um blog pessoal.

Devo usar o WordPress, Ghost, Jekyll ou Hugo? A ferramenta não importa! Só comece a escrever!

{{< figure src="blog_tools.png" width=500px caption="[rakhim.org/honestly-undefined/19](https://rakhim.org/honestly-undefined/19/)">}}

### Analytics

Quando estiver criando seu cantinho da internet, evite os analytics comerciais, principalmente o Google Analytics. O Google Analytics está presente na esmagadora maioria dos sites, tenha carinho pelos seus leitores e considere uma alternativa mais privativa.

Já ouviu que “se o produto é de graça, você é o produto”? O produto de verdade do Google Analytics são os dados de navegação dos internautas. Que tal, pelo menos no seu cantinho, experimentar opções mais privativas?

Dentre as alternativas vou destacar o [goatcounter.com](https://www.goatcounter.com/): é gratuito para até 100k pageviews por mês e tem a opção self-hosting.

Para quem optar pela via de hospedar por sua conta o site, tem o [goaccess.io](https://goaccess.io/) que cria dashboards apartir dos logs de acessos dos webserver como Nginx e Apache, ou seja, zero scripts de rastreamento ^_^

### Usuários de RSS existem e resistem

Já falamos sobre RSS aqui no computando arte: [Utilizando o RSS para “furar a bolha” | Computando Arte](https://medium.com/computando-arte/utilizando-o-rss-para-furar-a-bolha-507f8b0ed410)

O RSS é um dos principais meios dos usuários acompanharem seu site, mas fique tranquilo pois provavelmente a ferramenta do seu site já deve gerar o feed sozinho.

Só se atente que o link do feed está presente no HTML, caso contrário a pessoa vai ter que “caçar” o seu feed, por exemplo:

{{< highlight html >}}
<link rel="alternate" type="application/rss+xml" href='https://caioau.net/blog/index.xml' title="Blogs on Pagina do caioau">
{{< /highlight >}}

{{< figure src="meme_rss.jpg" width=300px >}}

### Sobre ter um domínio próprio

_Vou criar um site, preciso ter um domínio?_ Absolutamente não precisa! Você pode usar a hospedagens gratuitas como Github Pages. Pessoalmente, prefiro o Gitlab ou até mesmo o Wordpress.

De novo, não se preocupe com isso! Só comece a escrever! Se depois de alguns meses ou anos que começou você sempre pode migrar para um domínio novo.Quando você for migrar, você pode colocar um atributo no HTML para redirecionar automaticamente para o domínio novo, o [http-equiv](https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element/meta#attr-http-equiv). Por exemplo:

{{< highlight html >}}
<meta http-equiv="refresh" content="0;url=https://caioau.net/" />
{{< /highlight >}}

Não caia nas extensões como .xyz! Pode parecer um preço tentador, mas muitas vezes o preço só é vantajoso para registrar o domínio na primeira vez, mas quando levar em conta as renovações não vale tanto a pena assim se comparado com extensões tradicionais como .net, .com ou .org.

_E um .br?_ O .br até que tem um preço bom, mas infelizmente no registro br seus dados pessoais (nome completo e CPF) ficam públicos no whois :/

### Um .onion (onion service) cai bem

Para o seu cantinho ficar ainda mais privado você pode disponibilizar seu site em um onion service, trazendo anonimato para quem o visita com o navegador Tor.

É tranquilo fazer! A documentação é muito boa: [community.torproject.org/onion-services](https://community.torproject.org/onion-services/). Dá pra facilitar ainda mais usando o [onionshare.org](https://onionshare.org/) (só apontar a pasta que fica os HTML).

Porém você vai precisar de um computador rodando 24 horas por dia para hospedar seu .onion, mas uma raspberry-pi dá conta do recado.

Último cabeçalho, prometo! Coloque no HTML o atributo http-equiv="onion-location”, dessa forma quando o navegador Tor abrir seu site na clearnet ele vai redirecionar automaticamente para a versão disponível no .onion, por exemplo:

{{< highlight html >}}
<meta http-equiv="onion-location" content="http://zgjjgd2o7zh4u76b5d5fpyekeif4nmbtbktufe7z4vaa6sazeraz4nyd.onion" />
{{< /highlight >}}

## /join #computando-arte

Quando estiver escrevendo seus textos é importante pedir feedbacks, dessa forma você verá como pessoas com diferentes bagagens receberão seu conteúdo, por isso ter um grupo de divulgação onde todo mundo se ajuda é muito bom!

Se gosta do nosso trabalho no Computando Arte e quiser escrever como convidado ou passar a compor o grupo, vamos te dar esse apoio! Basta nos enviar uma mensagem no twitter em [@ComputandoArte](https://twitter.com/ComputandoArte) ou no computando.arte@protonmail.com que iremos te responder :)
