---
title: "Slides: Testando playbooks com molecule"
date: 2022-10-03T16:00:00-03:00
draft: false
keywords: ["eldorado", "slides", "devops", "linux", "ansible"]
tags:  ["eldorado", "slides", "devops", "linux", "ansible"]
---

Atividade realizada dia 28 setembro 2022, no [Eldorado](https://www.eldorado.org.br/)

* [slides](slides-ansible-parte2.pdf) ([arquivo markdown editável](https://gitlab.com/caioau/caioau.gitlab.io/-/tree/master/static/blog/eldorado-ansible-molecule) feito com [Marp](https://marp.app/)).
