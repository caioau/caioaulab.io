---
title: "Slides: Como contêineres funcionam? parte 1"
date: 2022-01-05T16:00:00-03:00
draft: false
keywords: ["eldorado", "slides", "devops", "linux", "docker", "security"]
tags:  ["eldorado", "slides", "devops", "linux", "docker", "security"]
---

Atividade realizada dia 5 janeiro 2022, no [Eldorado](https://www.eldorado.org.br/)

* [slides](slides-containers_caioau_05jan2022.pdf) ([arquivo markdown editável](https://gitlab.com/caioau/caioau.gitlab.io/-/tree/master/static/blog/eldorado-intro-containers) feito com [Marp](https://marp.app/)).
