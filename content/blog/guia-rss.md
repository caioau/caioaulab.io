---
title: 'Utilizando o RSS para "furar a bolha"'
date: 2021-04-27T13:00:00-03:00
draft: false
keywords: ["dicas", "computando-arte", "rss", "Aaron-Swartz", "texto", "raspberry-pi","selfhost"]
tags:  ["dicas", "computando-arte", "rss", "Aaron-Swartz", "texto", "raspberry-pi", "selfhost"]
---

Obs.: Originalmente publicado no [computando-arte]({{< ref "/blog/computando-arte" >}}) dia 27Abr2021

Já percebeu que o seu feed da rede social está cheio de notícias que fazem você "passar raiva"? O fenômeno é chamado de doomscrolling, e apesar que de termos mais notícias desse tipo, as redes sociais as exibem com mais frequência.

A conhecida "bolha" (filter Bubble) das redes sociais faz com fiquemos mais tempo nas redes, exibindo apenas posts que engajamos mais, e isso está causando diversas consequências nefastas desde potencializar fake news e desinformação a ansiedade e depressão nos internautas.

Mas se eu te disser que existe uma alternativa? Um meio de receber as informações diretamente, sem nenhum filtro ou algoritmo. É disto que vamos falar hoje, o RSS.

{{< figure src="rss-logo.png" width=300px caption="Logo do RSS" >}}

## O que é o RSS?

RSS é a sigla para Really Simple Syndication, sincronização realmente simples, é um arquivo XML gerado por muitos sites que exibe os posts que o site tem, dessa forma a pessoa com um leitor de RSS consegue acompanhar diversos blog, sites de noticia e etc … centralizados no seu leitor.

## Quem criou o RSS?

Uma das pessoas responsáveis pela criação do RSS foi o Aaron Swartz, um hacker conhecido por diversas coisas como o Creative Commons e da melhor rede social o Reddit 😛

{{< figure src="Aaron-Swartz.jpeg" width=400px caption="Aaron Swartz (2008), Fonte: [wikimedia](https://commons.wikimedia.org/wiki/File:Aaron_Swartz_profile.jpg)" >}}

Quem se interessou pela historia do Aaron recomendo os seguintes conteúdos:

* O documentário: **O Menino da Internet: A História de Aaron Swartz**: [LibreFlix](https://libreflix.org/i/the-internets-own-boy)
* E pra quem gosta de podcast Engenharia Reversa #1: [Aaron Swartz, a história do ativista e co-criador do Reddit e do RSS](https://anchor.fm/engreversa/episodes/Aaron-Swartz--a-histria-do-ativista-e-co-criador-do-Reddit-e-do-RSS-1-eiif3j/a-a3166kn)

## Como usar o RSS? Parte 1: Os leitores

Para usar o RSS você precisa de um leitor de RSS, e não faltam opções! A primeira classe de leitores são os locais, um app local no seu computador ou celular que infelizmente não sincroniza entre seus dispositivos, se isso não é um problema para você é uma ótima opção.

O [thunderbird](https://www.thunderbird.net/) (cliente livre de emails) é a opção mais tradicional.

Gosta de programas que rodam no terminal? tem o [newsboat](https://newsboat.org/).

Para Linux tem o liferea que está disponível na maioria das distribuições.

No Android tem o [Feeder](https://play.google.com/store/apps/details?id=com.nononsenseapps.feeder.play), que também está disponível na [Fdroid](https://f-droid.org/) (loja de aplicativos livres).

Tem Mac ou iOS? Uma opção ótima é o [NetNewsWire](https://netnewswire.com/).

Depois desse monte de opções, se não quiser instalar nada 😛 tem o [Feedbro](https://nodetics.com/feedbro/) extensão para os navegadores (chrome e firefox).

Essas são todas as opções de leitores locais que conheço.

Vamos agora para as opções online, que permitem sincronizar entre seus dispositivos, é muito prático pois se viu algo interessante no computador pode marcar para ler depois no celular e vice versa.

O [Feedly](https://feedly.com/) é o serviço mais conhecido, tem um plano básico gratuito, bem amigável, para quem está começando recomendo ir com ele.

{{< figure src="feedly.png" width=900px caption="Captura de tela do Feedly" >}}

Por fim, a opção que eu uso, para quem gosta de software livre e se preocupa com sua privacidade, o Tiny Tiny RSS ([tt-rss.org](https://tt-rss.org/)). Você pode instalar numa raspberry-pi e acessar na sua rede local (caso precise acessar fora de casa pode usar serviços como [Pagekite](https://pagekite.net/) e [ngrok](https://ngrok.com/) para torna-ló acessível pela internet) ou alugar uma cloud e hospedar lá.

Outra opção para usar o tt-rss é utilizar a instancia do [Snopyta](https://snopyta.org/) (grupo sustentado por doações que hospeda diversos softwares livres).

## Como usar o RSS? Parte 2: Assinando os feeds

Com um leitor de RSS da sua preferencia funcionando, bora assinar os feeds!

Vamos começar pelo YouTube, os feeds são assim:

`www.youtube.com/feeds/videos.xml?channel_id=<id_do_canal>`

Substitua no final pelo channel_id do canal que quer assinar. Um truque para descobrir o id que tem interesse: Selecione um vídeo, em seguida clique no ícone do canal e daí o channel_id vai aparecer na url. Por exemplo, o feed do canal da Jana Viscardi é o seguinte:

`www.youtube.com/feeds/videos.xml?channel_id=UC9h2vDtQXEiD0O4aVubsYYA`

Caso o truque não funcione, com o vídeo aberto, abra o código fonte da página (use o atalho control+u) e procure (control+f) por **channelId**.

Para acessar os feeds da melhor rede social 😛, o Reddit:

Na [wiki tem uma seção sobre RSS](https://www.reddit.com/wiki/rss), basta colocar um .rss no final. Por exemplo o feed do [/r/dataisbeautiful](https://old.reddit.com/r/dataisbeautiful/) é:

`old.reddit.com/r/dataisbeautiful/.rss`

Precisa acompanhar projetos do github? Por exemplo, utilizo aqui em casa um firmware livre no meu roteador, o [OpenWRT](https://openwrt.org/), para acompanhar as releases o feed é:

`github.com/openwrt/openwrt/releases.atom`

No mesmo padrão para acompanhar os commits, da branch master:

`github.com/openwrt/openwrt/commits/master.atom`

Obs.: Os feeds atom na prática são a mesma coisa que feeds RSS, alguns sites adotam isso.

Gosta de Newsletters? O serviço [kill-the-newsletter.com](https://kill-the-newsletter.com/) cria um e-mail para você cadastrar na newsletter e um feed para assinar.

Veja o [diretório de Newsletters brasileiras](https://manualdousuario.net/newsletters-brasileiras/) que o manual do usuário fez.

Gosta do medium? o feed é assim: medium.com/feed/nome-do-usuario, por exemplo, o feed do computando arte 🤭 é:

`medium.com/feed/computando-arte`

E o Twitter? Infelizmente o twitter descontinuou os seus feeds RSS 😞 Mas tem um software livre chamado [Nitter](https://github.com/zedeus/nitter) que permite navegar pelos posts do twitter com privacidade e gerar um feed rss.

Tentei mostrar como chegar nos feeds de alguns serviços populares, mas e os outros? Então muitos sites e podcasts tem o ícone e/ou o link do seu feed, caso não encontre procure no código fonte da pagina por rss ou feed ou xml ou atom e você vai acabar encontrando. A maioria das tecnologias por trás dos sites como o super popular WordPress gera nativamente os feeds RSS.

Por exemplo, até meu site pessoal simples uso o [Hugo](https://gohugo.io/) (gerador de sites estáticos, semelhante ao [Jekyll](https://jekyllrb.com/)) e coloquei o link pro meu feed, mas se for procurar no código fonte da página encontrará:

{{< highlight html >}}
<link rel="alternate" type="application/rss+xml" href='https://caioau.net/blog/index.xml' title="Blogs on Pagina do caioau">
{{< /highlight >}}

Pronto, aqui você encontra o meu feed RSS:

`caioau.net/blog/index.xml`

Por fim, não poderia fazer um texto sobre RSS sem falar do [RSS-Bridge](https://github.com/RSS-Bridge/rss-bridge), software livre que gera feeds RSS apartir de páginas do facebook, instagram (dá até pra seguir hashtags ✨) , Wikipedia e várias outras integrações.

## Considerações finais

Com o RSS podemos "furar a bolha" e receber toda a informação sem filtros e algoritmos. Usando o RSS logo vai perceber uma das razões (além do lucro das big tech 💸) do porque desses filtros, é muita informação! Então use com moderação (para evitar o famoso FOMO, fear of missing out, a sensação de que não posso ficar minutos longe do meu celular pois estou perdendo algo) e tentar uma relação mais saudável com a tecnologia.

Vale recomendar o episódio #41 do tecnocracia, é um podcast excelente sobre as consequências da tecnologia e vou indicar justamente o ep. #41: [Manual prático para retomar sua atenção do calabouço das redes sociais](https://manualdousuario.net/podcast/tecnocracia-41/) com várias dicas 🧘🔕 de como controlar o celular e as redes sociais e não o contrário.

{{< figure src="phone-addiction.jpg" width=300px caption="Estamos usando a tecnologia? Ou sendo usados? ([Fonte](https://www.gizmodo.com.au/2014/09/a-vivid-reminder-from-banksy-that-technology-can-enslave-us-all/))" >}}

Por fim, pra quem gosta do tema, vou indicar alguns conteúdos muito interessantes 🤯

{{< figure src="meme-SocialDillema.jpeg" width=500px caption="Crédito: Cryptorave no [twitter](https://twitter.com/cryptoravebr/status/1307305692185817094?s=19)">}}

O documentário [Dilema nas Redes (2020)](https://www.imdb.com/title/tt11464826/), disponível na Netflix, bombou! Adoro como ele é muito didático e expões as principais questões do tema, recomendo muito pra quem ainda não viu. Outro doc, é o [Coded Bias (2020)](https://www.imdb.com/title/tt11394170/) que foca mais nas tecnologias de reconhecimento facial e seus vieses e contou com a presença da Cathy O'Neil, autora do livro fenomenal Weapons of Math Destruction traduzido no Brasil como Algoritmos de Destruição em Massa.

Uma talk recente, que eu adorei foi a [Rage Against The Machine Learning](https://media.ccc.de/v/rc3-891673-rage_against_the_machine_learning), onde o Dr. Hendrik Heuer fala da sua pesquisa de auditoria dos algoritmos das redes sociais para o interesse público.

Para podcast, vou indicar outros episódios do tecnocracia, é difícil indicar só alguns, todos são ótimos:
* #18: [Em nome do lucro, o YouTube abriu os portões do inferno da desinformação](https://manualdousuario.net/podcast/tecnocracia-18/)
* #40: [O filtro alucinógeno do Instagram colocou a humanidade no divã - e alguns no caixão](https://manualdousuario.net/podcast/tecnocracia-40/)
* #42: [As empresas que nos colocaram nessa enrascada não nos tirarão dela](https://manualdousuario.net/podcast/tecnocracia-42/)
