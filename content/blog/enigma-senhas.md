---
title: "Slides: O elo mais fraco da nossa segurança: S&nh5s"
date: 2019-09-12T16:10:14-03:00
draft: false
keywords: ["Enigma-Unicamp", "diceware", "passwords", "2FA", "password-manager","slides"]
tags: ["Enigma-Unicamp", "diceware", "passwords", "2FA", "password-manager","slides"]
---

Atividade realizada dia 12 setembro 2019, no [Enigma Unicamp](https://enigma.ic.unicamp.br/)

* [slides](senhasEnigmaSet2019.pdf) ([codigo fonte dos slides](https://github.com/caioau/caioau-personal/blob/master/website/slides/senhasEnigmaSet2019.md))
