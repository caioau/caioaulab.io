---
title: "Slides: Um guia de segurança e privacidade dos mochileiros das interwebs"
date: 2019-08-17T16:10:14-03:00
draft: false
keywords: ["casahacker", "privacidade", "slides"]
tags: ["casahacker", "privacidade", "slides"]
---

Atividade realizada dia 17 agosto 2019, na [Casa Hacker](https://casahacker.org/)

* [slides](casaHacker2019ago.pdf) ([codigo fonte dos slides](https://github.com/caioau/caioau-personal/blob/master/website/slides/casaHacker2019ago.md))
